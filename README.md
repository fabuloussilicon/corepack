#** CorePack version 0.90 **
##**This Repository contains everything you need to start loading cores onto your Cyber Cortex AV ** 
###CorePack is a utility for loading bitstreams into the FPGA of the Cyber CortexAV.

##  **How do I get it?**
###From the **Downloads** page
First Navigate to the `Downloads` tab at this repo.

Then click the file corresponding to your Operating System.

Follow the instructions provided below for your specific OS.

###From the command line
`git clone https://bitbucket.org/fabuloussilicon/corepack`




# Linux 
### Using the Linux installer
Before using the corepack installer make sure you distribution is supported.

The current supported distributions are:

* Arch
* Debian 
* Ubuntu
* Xubuntu
* Kubuntu
* Lubuntu
* Linux Mint
* Elementary OS
* Fedora
* CentOS

compatibility assumes that you still use the default package manager.

If you would like to use the installer there are two options.
Either click `corepack-0.90_Linux_installer.run` located inside the `.zip` on the downloads page and then select `run` or `execute`.
Alternatively you can run `./corepack-0.90_Linux_installer.run` inside a terminal.

if you cloned the repository then run `./corepack-install` from the `Linux/` directory in a terminal or click it and select `Run in terminal`. 

proceed with the installer.


###Linux Tips
#### Updating
Updating will be the exact same as the first install. Simply download the latest `.run` and an your package manager will update accordingly.

#### Debian, Ubuntu, Xubuntu, Kubuntu, Lubuntu, Linux Mint & Elementary OS
Use the `apt` or `dpkg` package manager with CorePack. 
This example shows getting package information...

###### EX.
`apt-cache show corepack`

or

`dpkg -l corepack`

both will work.

####Fedora & CentOS

Use the `yum` or `rpm` package manager with CorePack.
This example shows getting package information...
   
######EX.
```yum info corepack``` 

or

```rpm -qi corepack```

both will work.
    
### Without the Linux installer
If you are choosing not to use the installer because your distro is not supported then you make copy it to a folder
of you choosing. Make sure you have the `libftdi` driver, the`mono`  framework and `java 7` installed on you system.

Even though your distribution is not supported if you click the file `corepack-install` it will error and quit before installing but it will also generate a script in the `Linux/bin/` folder called `corepack` which you can use to run corepack.

Another way to run CorePack is directly from `mono`.
While in the directory you installed `corepack` run `mono CorePack.exe`.Additionally, give it a `.bit` as an argument to start the miniloader.

 Of course if your distribution is not supported we'd like to know so we can improve your experience with Fabulous Silicon. However, if your distribution is not supported there may be a reason. To find out contact the current maintainer.

Max Mansfield :  max@fabuloussilicon.com


##OS X 
Download `Cyber Cortex AV CorePack 0.90 for Mac OSX.pkg`

Click the file you have downloaded and proceed with the installer.

##Windows
Copy the cloned files to your programs directory such as`C:\Program Files\corepack`

Create a shortcut to the desktop if you wish.